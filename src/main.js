import Vue from 'vue';
import App from './App.vue';
import Vuetify from 'vuetify';
import theme from '@dealeron/design-system/src/theme/theme';

import './stylus/main.styl';

Vue.config.productionTip = false

Vue.use(Vuetify, { theme });

new Vue({
  render: h => h(App),
}).$mount('#app')
